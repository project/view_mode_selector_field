<?php

namespace Drupal\view_mode_selector_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'View Mode Selector Field Formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "view_mode_selector_field_formatter",
 *   label = @Translation("View Mode Selector Field Formatter"),
 *   field_types = {
 *     "view_mode_selector_field_type"
 *   }
 * )
 */
class ViewModeSelectorFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#value' => $item->value,
      ];
    }

    return $element;
  }

}
