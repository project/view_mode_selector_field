<?php

namespace Drupal\view_mode_selector_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'view_mode_selector_field_type' field type.
 *
 * @FieldType(
 *   id = "view_mode_selector_field_type",
 *   label = @Translation("View Mode Selector"),
 *   category = @Translation("General"),
 *   default_widget = "view_mode_selector_field_widget",
 *   default_formatter = "view_mode_selector_field_formatter"
 * )
 *
 * @DCG
 * If you are implementing a single value field type you may want to inherit
 * this class form some of the field type classes provided by Drupal core.
 * Check out /core/lib/Drupal/Core/Field/Plugin/Field/FieldType directory for a
 * list of available field type implementations.
 */
class ViewModeSelectorFieldType extends FieldItemBase {

  /**
   * ID.
   *
   * @const string
   */
  const ID = 'view_mode_selector_field_type';

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [
      'value' => DataDefinition::create('string')
        ->setLabel(t('Text value'))
        ->setRequired(FALSE),
    ];

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'not null' => FALSE,
          'description' => 'View mode name.',
          'length' => 255,
        ],
      ],
    ];

    return $schema;
  }

}
