<?php

namespace Drupal\view_mode_selector_field\Plugin\Field\FieldWidget;

use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'view_mode_selector_field_widget' field widget.
 *
 * @FieldWidget(
 *   id = "view_mode_selector_field_widget",
 *   label = @Translation("View Mode Selector Field Widget"),
 *   field_types = {"view_mode_selector_field_type"},
 * )
 */
class ViewModeSelectorFieldWidget extends WidgetBase {

  /**
   * Field directory.
   *
   * @const string
   */
  const FIELD_DIR = 'dir';

  /**
   * Theme.
   *
   * @const string
   */
  const FIELD_THEME = 'theme';

  /**
   * Group.
   *
   * @const string
   */
  const FIELD_GROUP = 'group';

  /**
   * Row items field.
   *
   * @const string
   */
  const FIELD_ROW = 'row';

  /**
   * Theme Handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal root.
   *
   * @var string
   */
  protected $root;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    ThemeHandlerInterface $themeHandler,
    FileSystemInterface $fileSystem,
    string $root
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->themeHandler = $themeHandler;
    $this->fileSystem = $fileSystem;
    $this->root = $root;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('theme_handler'),
      $container->get('file_system'),
      $container->getParameter('app.root')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      static::FIELD_DIR => 'view_mode_thumbnails',
      static::FIELD_THEME => static::getDefaultActiveTheme(),
      static::FIELD_GROUP => 'default',
      static::FIELD_ROW => 3,
    ] + parent::defaultSettings();
  }

  /**
   * Return the current active theme.
   */
  public static function getDefaultActiveTheme() {
    return \Drupal::config('system.theme')->get('default');
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element[static::FIELD_DIR] = [
      '#type' => 'textfield',
      '#title' => $this->t('Thumbnails directory (in the front theme)'),
      '#default_value' => $this->getSetting(static::FIELD_DIR),
      '#required' => TRUE,
    ];

    $element[static::FIELD_THEME] = [
      '#type' => 'select',
      '#options' => $this->getThemeList(),
      '#title' => $this->t('The theme where thumbnails are stocked'),
      '#default_value' => $this->getSetting(static::FIELD_THEME),
      '#required' => TRUE,
    ];

    $element[static::FIELD_GROUP] = [
      '#type' => 'select',
      '#options' => $this->getGroupList($form),
      '#title' => $this->t('The group where thumbnails are stocked'),
      '#default_value' => $this->getSetting(static::FIELD_GROUP),
      '#required' => TRUE,
    ];

    $element[static::FIELD_ROW] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 5,
      '#step' => 1,
      '#title' => $this->t('Number of thumbnails by row'),
      '#default_value' => $this->getSetting(static::FIELD_ROW),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [
      $this->t('Theme : @theme, Dir: @dir, Group: @grp', [
        '@theme' => $this->getSetting(static::FIELD_THEME),
        '@dir' => $this->getSetting(static::FIELD_DIR),
        '@grp' => $this->getSetting(static::FIELD_GROUP),
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Get the dir path according to settings.
    try {
      $dir_path = $this->getDirPath($items);
    }
    catch (\Exception $e) {
      $dir_path = NULL;
    }

    if ($dir_path) {
      // Get Image path from dir path.
      $img_paths = $this->getImgPath($dir_path);

      // Build the form.
      if (count($img_paths) > 1) {
        $default_value = $items->getValue()[$delta]['value'] ?? NULL;
        if (!$default_value) {
          $definition = $items->getFieldDefinition();
          $default_value = $definition->get('default_value')[0]['value'] ?? NULL;
        }
        $element = $this->getFormElement($element, $img_paths, $default_value);
      }
    }
    else {
      $element = [];
    }

    return $element;
  }

  /**
   * Return the list of available themes.
   *
   * @return array|string[]
   *   THe list of available themes name.
   */
  protected function getThemeList() {
    return array_map(function (Extension $extension) {
      return $extension->getName();
    }, $this->themeHandler->listInfo());
  }

  /**
   * Return the list of folders which on the folder of the bundle.
   *
   * @param array $form
   *   The form.
   *
   * @return array
   *   The list of available groupes name.
   */
  protected function getGroupList(array $form): array {
    try {
      $theme = $this->themeHandler->getTheme($this->getSetting(static::FIELD_THEME));
    }
    catch (\Exception $e) {
      $theme = $this->themeHandler->getTheme(static::getDefaultActiveTheme());
    }

    $dir = implode('/', [
      $this->root,
      $theme->getPath(),
      $this->getSetting(static::FIELD_DIR),
      $form['#entity_type'],
      $form['#bundle'],
    ]);

    $group_dir = $dir . '/' . $this->getSetting(static::FIELD_GROUP);

    if (!is_dir($group_dir)) {
      $this->fileSystem->prepareDirectory($group_dir, FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY);
    }

    $scan = scandir($dir);
    $folders = array_filter($scan, function (string $folder) {
      return ($folder != '..' && $folder != '.');
    });

    return array_combine($folders, $folders);
  }

  /**
   * Return the dir path according to settings.
   *
   * @return string|bool
   *   THe dir path or false if the dirpath does not exist.
   */
  protected function getDirPath(FieldItemListInterface $itemList) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $parent_entity */
    $parent_entity = $itemList->getParent()->getValue();

    // Concanate path parts.
    $dir = implode('/', [
      $this->root,
      $this->themeHandler->getTheme($this->getSetting(static::FIELD_THEME))
        ->getPath(),
      $this->getSetting(static::FIELD_DIR),
      $parent_entity->getEntityTypeId(),
      $parent_entity->bundle(),
      $this->getSetting(static::FIELD_GROUP),
    ]);

    return is_dir($dir) ? $dir : FALSE;
  }

  /**
   * Return the list of thumbnails path.
   *
   * @param string $dir_path
   *   Tje dir path.
   *
   * @return string[]
   *   List of image paths.
   */
  protected function getImgPath(string $dir_path) {
    $img_paths = $this->fileSystem->scanDirectory($dir_path, '*.*', ['recurse' => FALSE]);

    // Order imgPaths by the name of the pictures.
    $this->sortImgPath($img_paths);

    return $img_paths;
  }

  /**
   * Return the form element.
   *
   * @param array $element
   *   The element.
   * @param array $img_paths
   *   The list of img path.
   * @param mixed|null $default_value
   *   The default value.
   *
   * @return array
   *   The form element.
   */
  protected function getFormElement(array $element, array $img_paths, $default_value): array {
    // Attach libraries.
    $element['value']['#attached']['library'][] = 'view_mode_selector_field/view_mode_selector_field';

    if (!isset($default_value) && !empty($img_paths)) {
      $default_img_name = reset($img_paths)->name;
      $default_value = $this->getSetting(static::FIELD_GROUP) . '/' . $this->getValueFromImgName($default_img_name);
    }

    // Build input.
    $element['#type'] = 'item';
    $element['value']['#type'] = 'radios';
    $element['value']['#prefix'] = '<div class="view-mode-selector-field-wrapper" data-col="' . $this->getSetting(static::FIELD_ROW) . '">';
    $element['value']['#suffix'] = '</div>';
    $element['value']['#default_value'] = $default_value;
    $element['value']['#options'] = [];
    foreach ($img_paths as $imgData) {

      $value = $this->getValueFromImgName($imgData->name);
      $element['value']['#options'][$this->getSetting(static::FIELD_GROUP) . '/' . $value] =
        sprintf('<img src="%s" />',
                str_replace(\DRUPAL_ROOT, '', $imgData->uri)
        );
    }

    return $element;
  }

  /**
   * Order imgPaths by the name of pictures.
   *
   * @param array $img_paths
   *   The list of img path.
   */
  protected function sortImgPath(array &$img_paths) {
    uasort($img_paths, function ($a, $b) {
      $a = basename($a->uri);
      $b = basename($b->uri);
      if ($a == $b) {
        return 0;
      }

      return ($a < $b) ? -1 : 1;
    });
  }

  /**
   * Return value from name.
   *
   * Delete order from "--" separator.
   *
   * @param string $name
   *   The file name.
   *
   * @return mixed|string
   *   The value.
   */
  protected function getValueFromImgName($name) {
    $name_data = explode('--', $name);
    if (count($name_data) > 1) {
      $name = implode('--', array_slice($name_data, 1));
    }

    return $name;
  }

}
