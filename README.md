CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration

INTRODUCTION
------------

The View Mode Selector Field Module allows editors to select a view mode for
the parent entity thanks to a list of thumbnails images.
Developers can add thumbnails images in a specific folder
(theme/view_mode_thumbnails/{entity_type_id}/{bundle}/{group}) in the wanted
theme.
Once done, editors will see these images in the edition form of the entity. They
then will be able to select one image as a view mode.
Then the module will add a suggestion part in the template of the edited entity
according to the selection of the editors. For example, if the editor select
an image called
"theme/view_mode_thumbnails/{entity_type_id}/{bundle}/{group}/left.png"
then a suggestion prefixed by 'f_' will be added in the suggestions list. Ex:
```html
<!-- FILE NAME SUGGESTIONS:
   * paragraph--title-text-img--default--f-left--group.html.twig
   * paragraph--title-text-img--default--f-left.html.twig
   * paragraph--title-text-img--f-left--group.html.twig
   * paragraph--title-text-img--f-left.html.twig
   * paragraph--title-text-img--default.html.twig
   * paragraph--default.html.twig
   x paragraph--title-text-img.html.twig
   * paragraph.html.twig
-->
```

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/view_mode_selector_field

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/view_mode_selector_field

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Add this module in a component:

* Add in the form (ex: paragraph):
  - Go to: Admin » Structure » Paragraph types.
  - Choose your Paragraph types and click on 'manage the fields' and click on '
    Add a field'.
  - Select on 'Add a new field' the element 'ViewModeSelectorField' and assigns
    it a label.
  - Save this.
  - Go to the tab 'Manage the display of the form' and move the field on the
    active field of the form.
  - If you click on the gear of the field you can modify the name of the
    directory, which contains the thumbnails of suggestions (ex:
    view_mode_thumbnails), and the theme used.

* Add a suggestion for a component (ex: paragraph):
  - Go to your directory of your theme selected and create a folder named by the
    name chose (ex: view_mode_thumbnails).
  - In this folder you can create a folder with the name of the type of
    component (ex: 'paragraph').
  - In this folder you can create another folder with the name of the element
    which contain the field (ex: 'my_paragraph').
  - You can stock your thumbnails here. Each thumbnail is a suggestion for your
    theme.

* Add a template for a suggestion:
  - On the folder of your templates of your component (ex: my_theme » templates
    » components » paragraphs » my-paragraph ).
  - You can add a template. There is a name convention '
    component--my-component--f-my-thumbnail.twig'.
  - Here, we have the template 'paragraph--my-paragraph--f-my-thumbnail.twig'.
